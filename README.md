# Repositorio Git Grupo 5 - Aplicações Distribuídas

## Lider

* Marcella Toledo

## Componentes

* Gabriela Mansur
* Rodrigo Marden
* Julio Cesar
* Rodrigo Marden
* Ygor Alberto
* Matheus Assis

## Ajudas

    ### Erro no inicio da emulação da MV
        * No framework de emulação clicar em Configurações, guia USB, desabilitar USB

    ### logar com privilégios
        * su
        * senha da MV (Nossa está como 123456)
   
    ### atualizar Aptitude (atualizador)
        * aptitude update
    
    ### instalar git
        * aptitude install git
        
        ## aqui tem que sair do usuario root, usar o sem privilegios, apenas da exit
    
    ### atualizar configurações de perfil do git
        * git config --global user.name "Seu nome"
        * git config --global user.email "seuemail@host.com.br"
        * git config --global core.editor nano - editor texto padrão para o git (pode ser outro)
        
    ### mostrar dados do perfil do git
        * git config --global --list
        
    ### clonar o repositorio do Gitlab
        * git clone <url>
            * essa url está disponivel na aba Project logo abaixo do titulo
            
    ### lista e lê itens de um diretorio
        * ls
        * cd
        
    ### le a pasta do nosso repositorio
        * cd p1-g5
 
    ### abre no nano
	* nano <nomedoarquivo.extensao>

    ### atualiza o diretorio conforme o repositorio no Gitlab
	* git pull

    ### atualiza o repositorio no Gitlab conforme seu diretorio (modificação ou criação de arquivos)
	* git add <nomedoarquivo>
		* ou git add . para adiconar tudo
	* git status
		* este comando você verifica se o que atualizou/ criou entrou na fila de commits
	* git commit
		* adiciona comentarios no log (sem #)
	* git push origin master
		* exige senha, e eh a do seu Gitlab   
    
   ### entrar no server.js
	* atualizar o repositorio com o master
	* entrar na pasta server
	* na pasta server, digitar nodejs server.js
  
   ### testes com telnet
	* telnet localhost <numero_porta> - está especificado no server.js e por enquanto é 6000    
        
    
