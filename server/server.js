//Biblioteca TCP
net = require('net');
//Importa os outros módulos
Nick = require('./nick.js'); 
User = require('./user.js');
Users = require('./users.js');
Kick = require('./kick.js');
PrivMsg = require('./privmsg.js');

// Cria um vetor de clientes e duas listas dinâmicas. Uma nicks e users 
var clients = {};
var nicks = {}; 
var users = {};

// Matheus
// Cria um TCP server
	net.createServer(function (socket) {
  // Coloca o nome do server como o endereço local + porta
  socket.name = socket.remoteAddress + ":" + socket.remotePort;
  // Coloca o cliente na lista de clientes (vetor)
  clients.push(socket);
  // Manda uma mensagem de boas vidas ao cliente que entrar
  socket.write("Seja bem vindo " + socket.name + "\n");
  // Manda a todos os clientes (broadcast) uma mensagem que o cliente entrou 
  broadcast(socket.name + " entrou no chat\n", socket);
  // Quem esta no servidor manda mensagem e todos recebem
  socket.on('data', function (data) {
	   // broadcast(socket.name + "> " + data, socket);
	// verifica aqui o comando
	analisar(data);
  });
  
  // Remove o cliente quando deixa o chat
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " deixou o chat.\n");
  });
  
  // Definicao da funcao broadcast
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Nao mandar para o cliente que mandou a mensagem
      if (client === sender) return;
      client.write(message);
    });
    process.stdout.write(message)
  }

  // verificar a entrada de string para execucao dos comandos
  function analisar(data){
	var mensagem = String(data).trim();

	var args = mensagem.split(" ");
	if (args[0] == "NICK" ) nick(args);
	else if (args[0] == "USER") user(args);
	else if (args[0] == "JOIN") join(args);
	else if (args[0] == "USERS") users(args);
	else if (args[0] == "KICK") kick(args);
	else if (args[0] == "PRIVMSG") privmsg(args);
	else socket.write("Comando inexistente. Consulte documentação");
	}

  

	}).listen(6000);
// Mensagem inicial do chat 
console.log("Chat na porta 6000\n");
  	
