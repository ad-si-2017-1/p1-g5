## Nick
### /nick nickname
### Utilizado para associar um nickname (apelido) para um user (cliente) ou trocar um já existente.
```javascript
var nick = {}; //Dentro do IRC
      function nick(args) {
       if ( ! args[1] ) { 
         socket.write("ERRO: Nickname nulo\n");
         return; 
       }
       else if ( nicks [ args[1] ] ) {
         socket.write("ERRO: Nickname já está sendo usado\n");
         return;
       }
       else {
        if ( socket.nick ) {  
         delete nicks[ socket.nick ];
        }
        nicks [ args[1] ] = socket.name;
        socket.nick = args[1];
       }
      socket.write("OK: O comando NICK foi executado com sucesso\n");
     }
```
     
     
     