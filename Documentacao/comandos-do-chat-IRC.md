COMANDOS DO CHAT (IRC)

Lembre-se que todos os comandos são precedidos por uma barra “/” e pode ser executado em qualquer janela.


/server
Ele se conecta ao servidor de IRC.
Exemplo: /server irc.irc-hispano.org

/nick
Permite-nos alterar nosso nick pelo qual voce indica
Exemplo: /nick Groucho

/list
Mostra-nos a lista de canais disponíveis. Podemos opcionalmente indicar um padrão de pesquisa
Exemplo: /list


/join
Permite-nos entrar em um canal particular
Exemplo: /join #irc-hispano

/part
Permite-nos sair de um canal concretamente

/partall
Permite-nos sair dos canais onde estamos.

/hop
Permite-nos sair e entrar de novo em um canal.

/quit
Nos desconecta de um Servidor IRC. Opcionalmente podemos incidar uma mensagem de saida
Exemplo: /quit
Exemplo: /quit vemo-nos um outro momento:)


## Comandos de Rede

/msg
Para falar com um usuário ou um canal.
Exemplo: / msg Groucho Olá, como está você?
Exemplo: / msg # irc-hispânica amigos do canal Hola!

/AMSG
Para falar simultaneamente em todos os canais que estão concetados.
Exemplo: /amsg Olá a todos!

/notice
/me
Permite fala em terceira pessoa dentro de um canal
Exemplo: /me vou sair

/ame
faz o mesmo que o mesmo comando anterior, mas em todos os canais que estamos conectados.

/query
Abre um Nick em  privado que nos indicamos.
Exemplo: /query consulta

## Solicitando Informação

/who
Mostra-nos uma informação sobre um canal no qual estemos sobre un nick
Exemplo: /who Groucho
Exemplo: /who #amizade

/whois
Mostra-nos uma informação mais específica sobre nick
Exemplo: /whois Luzdegas

/ison
/help
Mostra ajuda interna do mIRC.
sendo Operador de um Canal

/mode
Altera os modos de um canal
Exemplo: /mode #canal +o Groucho

/ban
Põe um nick ou à máscara indicada
Exemplo: /ban #canal alguem

/kick
Expulsa um usuario de um canal. Podemos, facultativamente, indicar-lhe um motivo de expulsão
Exemplo: /kick #canal x
Exemplo: /kick #canal x Não quero-o mais em  neste Canal

/invite
Convida um usuario a entrar no canal
Exemplo: /invite Julio #Brasil

/topic
Permite-nos alterar o TOPIC do canal

/omsg
Permite enviar uma mensagem a todos os Operadores do canal
Exemplo: /omsg #canal Agradável, desde este momento moderamos o canal

/onotice
Permite enviar uma notificação a todos os Operadores do canal
Exemplo: /onotice #canal temos hoje temos poucos usuarios!

##Outros comandos muito habituais

/away
Permite-nos entrar ou sair no sistema.
Exemplo: /away
Exemplo: /away neste momento não estou porque é hora de janta

/ignore
Permite ignorar a um usuario
Exemplo: /ignore Pedro

/dccchat
Permite abrir uma conversação privada com um outro usuario.
Exemplo: /dccchat Julio

/send
Permite enviar arquivos a outro usuario.
Exemplo: /send
Exemplo: /send Julio musica.mp3

/clear
Limpa a janela na qual executa o comando.

/clearall
Limpa todas as janelas.

/close -m
Fecha todos os privados que esta aberto.

/timestamp on/off
Ativa ou desativa a marca de tempos durante as conversações.

/ignore
Permite ignorar a um usuario
Exemplo: /ignore Pedro

/dccchat
Permite abrir uma conversação privada com um outro usuario.
Exemplo: /dccchat Julio

/send
Permite enviar arquivos a outro usuario.
Exemplo: /send
Exemplo: /send Julio musica.mp3

/clear
Limpa a janela na qual executa o comando.

/clearall
Limpa todas as janelas.

/close -m
Fecha todos os privados que esta aberto.

/timestamp on/off
Ativa ou desativa a marca de tempos durante as conversações.
